import shutil
import os
import datetime
import shutil
import rm.app

DEFAULT_TRASH_ADRESS = "/home/student/rm/trash"
DEFAULT_TRASH_SIZE = 100000
DEFAULT_INFO_ADRESS = "/home/student/rm/output.txt"

class Codes():
    GOOD = 0
    CONFLICT = 1
    BAD = 2
    NO_FILE = 3
    NO_SPACE = 4

    # simple remove
def remove(file_to_delete, trash_adress=DEFAULT_TRASH_ADRESS, trash_size=DEFAULT_TRASH_SIZE, info_adress=DEFAULT_INFO_ADRESS, dry=False):

    free_trash_size = 0
    if os.path.exists(file_to_delete):
        file_path = os.path.join(os.getcwd(), file_to_delete)
        files_in_trash = os.listdir(trash_adress)
        file_size = rm.app.get_file_size(file_to_delete) + os.path.getsize(file_to_delete)
        if (file_size + rm.app.get_file_size(trash_adress)) <= trash_size:
            for file_in_trash in files_in_trash:
                if os.path.basename(file_to_delete) == os.path.basename(file_in_trash):
                    file_to_delete = rename(file_to_delete, files_in_trash, dry)
            if not dry:
                with open(info_adress, "a") as record:
                    file_info = " ".join([os.path.basename(file_to_delete), file_path, "\n"])
                    record.write(file_info)
                try:
                    shutil.move(file_to_delete, trash_adress)
                    free_trash_size = trash_size - file_size - rm.app.get_file_size(trash_adress)
                except:
                    return Codes.BAD, file_path, trash_size
            file_path = os.path.join(trash_adress, os.path.basename(file_to_delete))
            return  Codes.GOOD, file_path, free_trash_size
        else:
            return Codes.NO_SPACE, file_path, free_trash_size
    else:
        return Codes.NO_FILE, file_to_delete, free_trash_size


    # if file with this name already e)(ist in trash
def rename(file_adress, files_in_trash, dry=False):
    count = 0
    good_name = False
    while not good_name:
        for file_in_trash in files_in_trash:
            if "".join([os.path.basename(file_adress), str(count)]) == os.path.basename(file_in_trash):
                count = count + 1
                good_name = False
                break
            good_name = True

    new_file_adress = "".join([file_adress, str(count)])
    if not dry:
        os.rename(file_adress, new_file_adress)
    file_adress = new_file_adress
    return file_adress


def restore(new_file_name, trash_adress=DEFAULT_TRASH_ADRESS, info_adress=DEFAULT_INFO_ADRESS, replace=False, dry=False):
    file_in_trash = os.path.join(trash_adress, new_file_name)
    adress_file = ""

    if os.path.exists(file_in_trash):
        adress_file = search_file_info(info_adress, new_file_name, dry)
        if os.path.exists(adress_file):
            if replace:
                if not dry:
                    delete_file(adress_file)
                    os.rename(file_in_trash, adress_file)
                return Codes.GOOD, adress_file
            else:
                with open(info_adress, "a") as record:
                    file_info = " ".join([os.path.basename(new_file_name), adress_file, "\n"])
                    record.write(file_info)
                return Codes.CONFLICT, adress_file
        else:
            try:
                if not dry:
                    if not os.path.exists(os.path.dirname(adress_file)):
                        os.makedirs(os.path.dirname(adress_file))
                    os.rename(file_in_trash, adress_file)
            except OSError:
                return Codes.BAD, adress_file

            return Codes.GOOD, adress_file

    else:
        return Codes.NO_FILE, adress_file


def search_file_info(info_adress, new_file_name, dry):
    adress_file = ""
    with open(info_adress, "r") as info:
        lines = [line.strip() for line in info]
    with open(info_adress, "w") as record:
        for line in lines:
            values = line.split(" ")
            if values[0] != os.path.basename(new_file_name):
                record.write(line+"\n")
            else:
                new_file_name = values[0]
                adress_file = values[1]
                if dry:
                    record.write(line+"\n")
    return adress_file


# return lists: deleted files, undeleted files, renamed files.
def remove_regular(dir_adress, regular_value, trash_adress=DEFAULT_TRASH_ADRESS, trash_size=DEFAULT_TRASH_SIZE, 								   info_adress=DEFAULT_INFO_ADRESS, dry=False):
    deleted_files = []
    undeleted_files = []
    renamed_files = {}
    try:
        files_in_dir = os.listdir(dir_adress)
        for file_in_dir in files_in_dir:
            if regular_value in os.path.basename(file_in_dir):
                target_file = os.path.join(dir_adress, file_in_dir)
                code, new_file_adress, _ = remove(target_file, trash_adress, trash_size,
                                               info_adress, dry)
                if os.path.basename(target_file) != os.path.basename(new_file_adress):
                    renamed_files[os.path.basename(target_file)] = os.path.basename(new_file_adress)
                if code == Codes.GOOD:
                    deleted_files.append(target_file)
                else:
                    undeleted_files.append(target_file)
        return Codes.GOOD, deleted_files, undeleted_files, renamed_files
    except:
        return Codes.BAD, deleted_files, undeleted_files, renamed_files


def clear_trash_by_day(day=0, trash_adress=DEFAULT_TRASH_ADRESS, info_adress=DEFAULT_INFO_ADRESS):
    deleted_files = []
    files_in_trash = os.listdir(trash_adress)
    for file_in_trash in files_in_trash:
        if datetime.datetime.today().isoweekday() == day:
            file_to_delete = os.path.join(trash_adress, file_in_trash)
            with open(info_adress, "r") as info:
                lines = [line.strip() for line in info]
            with open(info_adress, "w") as record:
                for line in lines:
                    values = line.split(" ")
                    if values[0] != os.path.basename(file_to_delete):
                        record.write(line+"\n")
            deleted_files.append(file_to_delete)
            delete_file(file_to_delete)
    return deleted_files


def clear_trash_by_size(size=5000, trash_adress=DEFAULT_TRASH_ADRESS,
                        info_adress=DEFAULT_INFO_ADRESS):
    deleted_files = []
    file_size = 0
    files_in_trash = os.listdir(trash_adress)
    for file_in_trash in files_in_trash:
        file_to_delete = os.path.join(trash_adress, file_in_trash)
        if os.path.isdir(file_to_delete):
            file_size = os.path.getsize(file_to_delete)
        file_size += get_file_size(file_to_delete)
        if file_size > size:
            with open(info_adress, "r") as info:
                lines = [line.strip() for line in info]
            with open(info_adress, "w") as record:
                for line in lines:
                    values = line.split(" ")
                    if values[0] != os.path.basename(file_to_delete):
                        record.write(line+"\n")
            deleted_files.append(file_to_delete)
            delete_file(file_to_delete)
    return deleted_files


def delete_file(file_to_delete):
    if os.path.isfile(file_to_delete):
        os.remove(file_to_delete)
    if os.path.isdir(file_to_delete):
        shutil.rmtree(file_to_delete)


def clear_trash_manual(files_to_delete, trash_adress=DEFAULT_TRASH_ADRESS,
                       info_adress=DEFAULT_INFO_ADRESS):
    deleted_files = []
    for file_in_trash in files_to_delete:
        file_to_delete = os.path.join(trash_adress, file_in_trash)
        with open(info_adress, "r") as info:
            lines = [line.strip() for line in info]
        with open(info_adress, "w") as record:
            for line in lines:
                values = line.split(" ")
                if values[0] != os.path.basename(file_to_delete):
                    record.write(line+"\n")
        deleted_files.append(file_to_delete)
        delete_file(file_to_delete)
    return deleted_files






