import os
import sys
import argparse
import ConfigParser
import rm.delete
import logging


LOG_FORMAT = "Time: %(asctime)s  Type: [%(levelname)s] Message: %(message)s"
LOGGER = logging.getLogger(__name__)
TERMINAL_MESSAGE = logging.StreamHandler()

def setup_log(adress_log, log_level="DEBUG", silent=False):
    if not silent:
        LOGGER.addHandler(TERMINAL_MESSAGE)
    try:
        LOGGER.setLevel(log_level)
        logging.basicConfig(filename=adress_log, format=LOG_FORMAT)
        return True
    except:
        return False


    # shows_files_in_trash
def show_trash_files(trash_adress, limit=10):
    size_of_trash = 0
    count_files = 0
    files_in_trash = get_trash_files(trash_adress)
    print "---------------//---------------"
    for file_in_trash in files_in_trash:
        size_of_file = files_in_trash[file_in_trash]
        size_of_trash += size_of_file
        message = " ".join([file_in_trash, "|Size:", str(size_of_file)])
        print message
        count_files = count_files + 1
        if count_files == int(limit):
            raw_input()
            count_files = 0
    message = " ".join(["Size of trash", str(size_of_trash)])
    print message
    print "---------------//---------------"


    # return the dictionary in which filename = key and calue = the size of this file
def get_trash_files(trash_adress):
    file_size = 0
    files_in_trash = {}
    try:
        for file_in_trash in os.listdir(trash_adress):
            file_path = os.path.join(trash_adress, file_in_trash)
            if os.path.isdir(file_path):
                file_size = os.path.getsize(file_path)
            file_size += get_file_size(file_path)
            files_in_trash[file_in_trash] = file_size
        return files_in_trash
    except:
        return files_in_trash


    # return size of file
def get_file_size(path):
    size = 0
    if os.path.isdir(path):
        for file_in_dir in os.listdir(path):
            file_for_size = os.path.join(path, file_in_dir)
            if os.path.isdir(file_for_size):
                size += os.path.getsize(file_for_size) + get_file_size(file_for_size)
            else:
                size += os.path.getsize(file_for_size)
    else:
        size += os.path.getsize(path)
    return size


    # loads_all_configs
def config_load():
    try:
	config = open("/home/student/New_Second_Lab/rm/config.cfg")
	arg_config_list = list()
	i = 0
	while i < 8:
            arg_config_list.append(config.readline())
	    i = i + 1
    except:
        message = "Error in config.cfg - use default configuration"
        print message

    help_string = ''
    help_string2 = ''
    elements_of_list = 0
    start_inf = 0
    end_inf = 0
    for element in arg_config_list :
	help_string = arg_config_list[elements_of_list]
	number = 0
	for simbol in help_string :
	    number = number + 1
	    if (simbol == '='): 
		start_inf = number + 1
	end_inf = len(help_string) - 1
	help_string2 = help_string[start_inf:end_inf]
	arg_config_list[elements_of_list] = help_string2
	elements_of_list = elements_of_list + 1

    info = arg_config_list[1]
    regular = arg_config_list[2]
    restore = arg_config_list[3]
    dry = arg_config_list[4]
    silent = arg_config_list[5]
    trash_adress = arg_config_list[6]
    info_adress = arg_config_list[7]
    if info == 'False' :
	info = False
    if regular == 'False' :
	regular = False
    if restore == 'False' :
	restore = False
    if dry == 'False' :
	dry = False
    if silent == 'False' :
	silent = False
    return info, regular, restore, dry, silent, trash_adress, info_adress


    #checking for system folders
def check_sys_dir(paths):
    for path in paths:
    # if /home/smth_system it'll crash
        arg_adress = os.path.abspath(path).split("/")
        if len(arg_adress) == 2 and arg_adress[0] == "":
	    LOGGER.log(50, message)
            return True
    # if /home/student/.../ - it'll OK
    return False


def write_remove_message(code, arg, new_adress, verbose=False):
    message = ""
    if code == rm.delete.Codes.BAD:
        message = " ".join(["Unknown error"])
        LOGGER.log(40, message)
    elif code == rm.delete.Codes.GOOD:
        if os.path.basename(arg) != os.path.basename(new_adress):
            message = " ".join(["File", os.path.basename(arg),
                                "renamed to", os.path.basename(new_adress)])
            LOGGER.log(20, message)
        message = " ".join(["The file", os.path.basename(arg), "is deleted"])
        if verbose:
            filesize = get_file_size(new_adress) + os.path.getsize(new_adress)
            message = " ".join([message, "| Size of file:", str(filesize)])
        LOGGER.log(10, message)
    elif code == rm.delete.Codes.NO_SPACE:
        message = " ".join(["The trash is no place for", os.path.basename(arg)])
        if verbose:
            filesize = get_file_size(arg) + os.path.getsize(arg)
            message = " ".join([message, "| Size of file:", str(filesize),
                                "| Free size of trash:", str(trash_space)])
        LOGGER.log(40, message)
    elif code == rm.delete.Codes.NO_FILE:
        message = " ".join(["This file:", os.path.basename(arg), "does not exist"])
        LOGGER.log(40, message)
    return message


def write_restore_message(code, arg, adress_file, verbose=False):
    message = ""
    if code == rm.delete.Codes.BAD:
        message = " ".join(["Information about this file:",
                            os.path.basename(arg), "has been lost"])
        LOGGER.log(50, message)
    elif code == rm.delete.Codes.GOOD:
        message = " ".join(["File", arg, "restore. Adress:", adress_file])
        if verbose:
            filesize = rm.delete.get_file_size(adress_file) + os.path.getsize(adress_file)
            message = " ".join([message, "| Size of file:", str(filesize)])
        LOGGER.log(10, message)
    elif code == rm.delete.Codes.CONFLICT:
        message = " ".join(["This file already exist:", os.path.basename(arg)])
        LOGGER.log(30, message)
    elif code == rm.delete.Codes.NO_FILE:
        message = " ".join(["Trash does not contain this file:", arg])
        LOGGER.log(40, message)
    return message


def write_regular_remove_message(code, arg, deleted_files, undeleted_files,
                                 renamed_files, verbose=False):
    message = ""
    if code == rm.delete.Codes.GOOD:
        for deleted_file in deleted_files:
            deleted_file = os.path.basename(deleted_file)
            for renamed_file in renamed_files:
                if deleted_file == renamed_file:
                    message = " ".join(["File", deleted_file, "renamed to",
                                        renamed_files[renamed_file]])
                    LOGGER.log(20, message)
            message = " ".join(["The file", deleted_file, "is deleted"])
            if verbose:
                filesize = rm.delete.get_file_size(deleted_file) + os.path.getsize(deleted_file)
                message = " ".join([message, "| Size of file:", str(filesize)])
            LOGGER.log(10, message)
        for file_adress in undeleted_files:
            message = " ".join(["The trash is no place for",
                                os.path.basename(file_adress)])
            if verbose:
                filesize = rm.delete.get_file_size(file_adress) + os.path.getsize(file_adress)
                message = " ".join([message, "| Size of file:", str(filesize)])
            LOGGER.log(40, message)
    else:
        message = " ".join(["This directory doesn't exist:", arg])
        LOGGER.log(40, message)
    return message


def main():

    # parser
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--info", action="store_true")
    parser.add_argument("-rg", "--regular", nargs="?", type = str)
    parser.add_argument("-rs", "--restore", action="store_true")
    parser.add_argument("--dry", action="store_true",)
    parser.add_argument("--silent", action="store_true")
    parser.add_argument("--show", nargs="?", const=10)
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("--day")
    parser.add_argument("--size")
    parser.add_argument("--manual", nargs="*", type=str)
    parser.add_argument("paths", nargs="*", type = str)
    args = parser.parse_args()

    # constants and variables
    message = ''
    info = False
    regular = False
    restore = False
    replace = False
    dry = False
    silent = False
    verbose = False
    trash_size = 100000
    log_level = "DEBUG"
    log_path = "/home/student/rmpy/log.log"
    trash_adress = "/home/student/rmpy/trash"
    info_adress = "/home/student/rmpy/output.txt"
    size_policy = False
    day_policy = False

    info, regular, restore, dry, silent, trash_adress, info_adress = config_load()
    if not os.path.exists(trash_adress):
        LOGGER.log(20, message)
        os.makedirs(trash_adress)
    if not os.path.exists(info_adress):
        with open(info_adress, "w"):
            pass
        message = " ".join(["Output.txt does not exist. Creating file", info_adress])
        LOGGER.log(20, message)

    # parse_processing
    if args.info:
        info = True
    if args.regular:
        regular = True
    if args.restore:
        restore = True
    if args.dry:
        dry = True
    if args.silent:
        silent = True
    if args.verbose:
        verbose = True
    if args.size:
        size = int(args.size)
        size_policy = True
    if args.day:
        day = int(args.day)
        day_policy = True

    # system and number of files check
    if check_sys_dir(args.paths):
        print "It seems like this is a system directory"
        sys.exit()
    # number of files check
    if len(args.paths) > 100:
        print "Out of range"
        LOGGER.log(50, message)
        sys.exit()
    # setup log check
    if not os.path.exists(log_path):
        with open(log_path, "w"):
            pass
        message = " ".join(["log.log does not exist. Creating file", log_path])
        LOGGER.log(20, message)
    if not setup_log(log_path, log_level, silent):
        print "Wrong log information"
        sys.exit()

    if args.paths == [] and args.regular:
        args.paths = [1]
        args.paths[0] = os.getcwd()

    # processing args
    answer = False
    for arg in args.paths:
        if args.info:
            message = " ".join(["Do you want to perform an operation with this file :", arg, "? [y/n]"])
            if raw_input(message) == "y":
                answer = True
            else:
                answer = False

        if ((answer and args.info) or (args.info == False)):
            if args.paths and not args.regular and not args.restore:
                code, new_adress, new_size = rm.delete.remove(arg, trash_adress, trash_size, info_adress, dry)
                write_remove_message(code, arg, new_adress, verbose)
	    if args.regular:
                code, deleted_files, undeleted_files, renamed_files = rm.delete.remove_regular(arg, args.regular,
                                                                      trash_adress, trash_size, info_adress, dry)
                write_regular_remove_message(code, arg, deleted_files, undeleted_files, renamed_files, verbose)
            if args.restore:
                code, adress_file = rm.delete.restore(arg, trash_adress, info_adress, replace, dry)
                write_restore_message(code, arg, adress_file, verbose)

    if args.show and not silent:
        show_trash_files(trash_adress, args.show)

    if size_policy:
        deleted_files = rm.delete.clear_trash_by_size(size, trash_adress, info_adress)
        for deleted_file in deleted_files:
            message = " ".join(["The file", os.path.basename(deleted_file), "is deleted"])
            LOGGER.log(10, message)

    if day_policy:
        deleted_files = rm.delete.clear_trash_by_day(day, trash_adress, info_adress)
        for deleted_file in deleted_files:
            message = " ".join(["The file", os.path.basename(deleted_file), "is deleted"])
            LOGGER.log(10, message)

    if args.manual:
        deleted_files = rm.delete.clear_trash_manual(args.manual, trash_adress, info_adress)
        for deleted_file in deleted_files:
            message = " ".join(["The file", os.path.basename(deleted_file), "is deleted"])
            LOGGER.log(10, message)


if __name__ == '__main__':
    main()








